package projectes;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Programa de Ajedrez_ProyectoM5_UF2 javadoc
 * 
 * @link https://gitlab.com/pcapitang/escacs
 * @author Pablo Captain Giraldo
 * 
 */
public class Escacs {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		cartel();
		System.out.println("Introduzca una letra de pieza: (C, A, T, D, P, R)");
		String pesa = sc.next().toLowerCase();
		while (!letra(pesa)) {
			System.err.println("Letra introducida no válida");
			pesa = sc.next().toLowerCase();
		}
		System.out.println("Introduzca una Posición: (Min: 11 - Max: 88)");
		System.err.println("Recuerda: Las posiciones que acaben en 9 o 0 no se permiten");

		int pos = sc.nextInt();
		while ((pos > 88 || pos > 11) && !limites(pos)) {
			System.err.println("Posición no válida");
			pos = sc.nextInt();
		}
		moviments(pos, pesa);
		sc.close();

	}

	public static void moviments(int casella, String pesa) {
		ArrayList<Integer> movs = new ArrayList<Integer>();
		switch (pesa) {
		case "c":
			movs.addAll(cavall(casella));
			System.out.println("Moviments cavall: " + movs);
			movs.clear();
			break;
		case "a":
			movs.addAll(alfil(casella));
			System.out.println("Moviments alfil: " + movs);
			movs.clear();
			break;
		case "t":
			movs.addAll(torre(casella));
			System.out.println("Moviments torre: " + movs);
			movs.clear();
			break;
		case "d":
			movs.addAll(dama(casella));
			System.out.println("Moviments dama: " + movs);
			movs.clear();
			break;
		case "p":
			movs.addAll(peo(casella));
			System.out.println("Moviments peo: " + movs);
			movs.clear();
			break;
		case "r":
			movs.addAll(rei(casella));
			System.out.println("Moviments rei: " + movs);
			break;
		}
	}

	public static ArrayList<Integer> cavall(int pos) {
		ArrayList<Integer> cavall = new ArrayList<Integer>();
		if (limites(pos+19))
			cavall.add(pos + 19); // arriba izquierda
		if (limites(pos+21))
			cavall.add(pos + 21); // arriba derecha
		if (limites(pos+12))
			cavall.add(pos + 12); // derecha arriba
		if (limites(pos-8))
			cavall.add(pos - 8); // derecha abajo
		if (limites(pos-19))
			cavall.add(pos - 19); // abajo izquierda
		if (limites(pos-21))
			cavall.add(pos - 21); // abajo derecha
		if (limites(pos-12))
			cavall.add(pos - 12); // izquierda abajo
		if (limites(pos+8))
			cavall.add(pos + 8); // izqierda arriba
		cavall.sort(null);
		return cavall;
	}
	public static ArrayList<Integer> alfil(int pos) {
		int npos = pos;
		ArrayList<Integer> alfil = new ArrayList<Integer>();
		while (limites(npos)) { // Diagonal derecha abajo
			npos -= 9;
			if (limites(npos) && npos >= 11 && npos <= 88)
				alfil.add(npos);
		}
		npos = pos;
		while (limites(npos)) { // Diagonal derecha arriba
			npos += 11;
			if (limites(npos) && npos >= 11 && npos <= 88)
				alfil.add(npos);
		}
		npos = pos;
		while (limites(npos)) { // Diagonal izquierda arriba
			npos += 9;
			if (limites(npos) && npos >= 11 && npos <= 88)
				alfil.add(npos);
		}
		npos = pos;
		while (limites(npos)) { // Diagonal izquierda abajo
			npos -= 11;
			if (limites(npos) && npos >= 11 && npos <= 88)
				alfil.add(npos);
		}
		alfil.sort(null);
		return alfil;
	}

	public static ArrayList<Integer> torre(int pos) {
		ArrayList<Integer> torre = new ArrayList<Integer>();
		int ini = pos / 10;
		int npos = ini * 10;
		for (int i = 0; i <= 8; i++) { //iquierda --> derecha
			if (limites(npos) && npos != pos)
				torre.add(npos);
			npos += 1;
		}
		ini = pos % 10;
		npos = ini + 10;
		for (int i = 0; i <= 8; i++) { //abajo ---> arriba
			if (limites(npos) && npos != pos)
				torre.add(npos);
			npos += 10;
		}
		torre.sort(null);
		return torre;
	}

	public static ArrayList<Integer> peo(int pos) {
		ArrayList<Integer> peo = new ArrayList<Integer>();
		if (limites(pos+10))
			if (pos / 10 != 2)
				peo.add(pos + 10);
			else {
				peo.add(pos + 10);
				peo.add(pos + 20);
			}
		peo.sort(null);
		return peo;
	}

	public static ArrayList<Integer> dama(int pos) {
		ArrayList<Integer> dama = new ArrayList<Integer>();
		dama.addAll(torre(pos));
		dama.addAll(alfil(pos));
		dama.sort(null);
		return dama;
	}

	public static ArrayList<Integer> rei(int pos) {
		ArrayList<Integer> rei = new ArrayList<Integer>();
		int npos = pos - 11;
		for (int i = 0; i < 3; i++) {
			if (limites(npos))
				rei.add(npos);
			npos += 1;
		}
		npos = pos - 1;
		for (int i = 0; i < 2; i++) {
			if (limites(npos) && npos != pos)
				rei.add(npos);
			npos = pos + 1;
		}
		npos = pos + 9;
		for (int i = 0; i < 3; i++) {
			if (limites(npos))
				rei.add(npos);
			npos += 1;
		}
		rei.sort(null);
		return rei;
	}
	public static boolean limites(int npos) {
		if (npos % 10 != 0 && npos % 10 != 9 && npos >= 11 && npos <= 88)
			return true;
		else {
			return false;
		}
	}

	public static boolean letra(String letra) {
		ArrayList<String> arr = new ArrayList<String>(6);
		arr.add("c");
		arr.add("a");
		arr.add("t");
		arr.add("d");
		arr.add("p");
		arr.add("r");
		if (arr.contains(letra))
			return true;
		else {
			return false;
		}

	}

	public static void cartel() {
		System.out.println("+------------------------------------------------+");
		System.out.println("|                                                |");
		System.out.println("|  +----  +----- +----  +----+  +----  +-----    |");
		System.out.println("|  |      \\      |      |    |  |      \\         |");
		System.out.println("|  |---     --   |      |    |  |        --      |");
		System.out.println("|  |          \\  |      |----|  |          \\     |");
		System.out.println("|  +----  -----+ +----  |    |  +----  -----+    |");
		System.out.println("|                                                |");
		System.out.println("+------------------------------------------------+");
		System.out.println("");
		System.out.println("");
	}
}