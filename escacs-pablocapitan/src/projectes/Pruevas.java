package projectes;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Pruevas {
	@Test
	public void testCavall() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(24, 26, 33, 37, 53, 57, 64, 66)), Escacs.cavall(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(23, 32)), Escacs.cavall(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(62, 73)), Escacs.cavall(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(67, 76)), Escacs.cavall(88));
		assertEquals(new ArrayList<Integer>(Arrays.asList(26, 37)), Escacs.cavall(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 34, 41, 43)), Escacs.cavall(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 33, 53, 62)), Escacs.cavall(41));
		assertEquals(new ArrayList<Integer>(Arrays.asList(63, 65, 72, 76)), Escacs.cavall(84));
		assertEquals(new ArrayList<Integer>(Arrays.asList(37, 46, 66, 77)), Escacs.cavall(58));
	}
	@Test
	public void testAlfil() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 18, 23, 27, 34, 36, 54, 56, 63, 67, 72, 78, 81)), Escacs.alfil(45));
		assertEquals(new ArrayList<Integer>(Arrays.asList(22, 33, 44, 55, 66, 77, 88)), Escacs.alfil(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 22, 33, 44, 55, 66, 77)), Escacs.alfil(88));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 27, 36, 45, 54, 63, 72)), Escacs.alfil(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(27, 36, 45, 54, 63, 72, 81)), Escacs.alfil(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(16, 25, 34, 41, 43, 61, 63, 74, 85)), Escacs.alfil(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 32, 43, 54, 65, 76, 87)), Escacs.alfil(21));
	}
	@Test
	public void testPeo() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(31, 41)), Escacs.peo(21));
		assertEquals(new ArrayList<Integer>(Arrays.asList(32, 42)), Escacs.peo(22));
		assertEquals(new ArrayList<Integer>(Arrays.asList(33, 43)), Escacs.peo(23));
		assertEquals(new ArrayList<Integer>(Arrays.asList(34, 44)), Escacs.peo(24));
		assertEquals(new ArrayList<Integer>(Arrays.asList(35, 45)), Escacs.peo(25));
		assertEquals(new ArrayList<Integer>(Arrays.asList(36, 46)), Escacs.peo(26));
		assertEquals(new ArrayList<Integer>(Arrays.asList(37, 47)), Escacs.peo(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(38, 48)), Escacs.peo(28));

		assertEquals(new ArrayList<Integer>(Arrays.asList(62)), Escacs.peo(52));
		assertEquals(new ArrayList<Integer>(Arrays.asList(77)), Escacs.peo(67));
		assertEquals(new ArrayList<Integer>(Arrays.asList(81)), Escacs.peo(71));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Escacs.peo(85));
		assertEquals(new ArrayList<Integer>(Arrays.asList()), Escacs.peo(88));
	}
	@Test
	public void testTorre() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 24, 34, 44, 51, 52, 53, 55, 56, 57, 58, 64, 74, 84)),Escacs.torre(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 21, 22, 23, 24, 25, 26, 28, 37, 47, 57, 67, 77, 87)),Escacs.torre(27));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 23, 33, 43, 53, 63, 73, 81, 82, 84, 85, 86, 87, 88)),Escacs.torre(83));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 13, 14, 15, 16, 17, 18, 21, 31, 41, 51, 61, 71, 81)),Escacs.torre(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(18, 28, 38, 48, 58, 68, 78, 81, 82, 83, 84, 85, 86, 87)),Escacs.torre(88));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 21, 31, 41, 51, 61, 71, 82, 83, 84, 85, 86, 87, 88)),Escacs.torre(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 15, 16, 17, 28, 38, 48, 58, 68, 78, 88)),Escacs.torre(18));
	}
	@Test
	public void testDama() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 13, 14, 15, 16, 17, 18, 21, 22, 31, 33, 41, 44, 51, 55, 61, 66, 71, 77, 81, 88)),
				Escacs.dama(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 18, 22, 28, 33, 38, 44, 48, 55, 58, 66, 68, 77, 78, 81, 82, 83, 84, 85, 86, 87)),
				Escacs.dama(88));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 18, 21, 27, 31, 36, 41, 45, 51, 54, 61, 63, 71, 72, 82, 83, 84, 85, 86, 87, 88)),
				Escacs.dama(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 15, 16, 17, 27, 28, 36, 38, 45, 48, 54, 58, 63, 68, 72, 78, 81, 88)),
				Escacs.dama(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(14, 18, 21, 24, 27, 32, 34, 36, 43, 44, 45, 51, 52, 53, 55, 56, 57, 58, 63, 64, 65, 72, 74, 76, 81, 84, 87)),
				Escacs.dama(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(11, 12, 13, 14, 16, 17, 18, 24, 25, 26, 33, 35, 37, 42, 45, 48, 51, 55, 65, 75, 85)),
				Escacs.dama(15));
	}
	@Test
	public void testRei() {
		assertEquals(new ArrayList<Integer>(Arrays.asList(43, 44, 45, 53, 55, 63, 64, 65)), Escacs.rei(54));
		assertEquals(new ArrayList<Integer>(Arrays.asList(12, 21, 22)), Escacs.rei(11));
		assertEquals(new ArrayList<Integer>(Arrays.asList(17, 27, 28)), Escacs.rei(18));
		assertEquals(new ArrayList<Integer>(Arrays.asList(71, 72, 82)), Escacs.rei(81));
		assertEquals(new ArrayList<Integer>(Arrays.asList(77, 78, 87)), Escacs.rei(88));
		assertEquals(new ArrayList<Integer>(Arrays.asList(13, 15, 23, 24, 25)), Escacs.rei(14));
	}
}
